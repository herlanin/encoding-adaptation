addpath('util')
addpath(genpath('mex'))
setenv('LD_LIBRARY_PATH','/usr/local/lib');
Dir   = pwd;
path_of_encoding=fullfile(Dir,'encoding\');
temp=fullfile(Dir,'\temp\');

methods={'fv'};
% methods={'fv'};
codebooks_sizes={128,256,512,1024};
% if createCodebooks(path,_of_encoding,codebooks_sizes)
    start(path_of_encoding,methods,codebooks_sizes);
% else    
%     fprintf('Something went wrong...');
% end
%  