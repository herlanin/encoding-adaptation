function start(path_of_encoding,methods,codebooks_sizes)
    for a=1:numel(codebooks_sizes)
       codebook.size= codebooks_sizes{a};
       
           for b=1:numel(methods)
                encoder.type=methods{b};
                midlevelpath=fullfile(path_of_encoding,int2str(codebook.size),'\encoded\',encoder.type);
                features_path=fullfile(path_of_encoding,int2str(codebook.size),'\descriptors_classes-codebook\');
                flist=dir(features_path);
                for i=3:length(flist)
                   if flist(i).isdir
                       descriptor_path=strcat(features_path,flist(i).name);
                        feature_files=dir(descriptor_path);
                        namestrn={};
                        names={};
                        for j=1:length(feature_files)
                       name_split=strsplit(feature_files(j).name,'_');
                       if string(name_split{1})=='train'
                          namestrn{end+1}=feature_files(j).name;
                       end
                        end
                        for j=1:length(feature_files)
                           if ~feature_files(j).isdir
                               names{end+1}=feature_files(j).name;
                           end
                        end
                           

    %                      codebook_path=strcat(descriptor_path,'\codebook');
                         num_time_series_train = numel(namestrn);
                         num_time_series = numel(names);
                        descriptorFile = fullfile(descriptor_path,'\test_0');
                        dt =load(descriptorFile);
                        t=size(dt,1);
                         sample_size=t*num_time_series_train*30/100;
                         classes_path=strcat(features_path,flist(i).name,'\classes');
                         codebook.path=fullfile(path_of_encoding,int2str(codebook.size),'\descriptors_classes-codebook\');
%                          encoded_features_path=strcat(pwd,'\encoding\midlevel\',int2str(codebook.size),'\midlevel_representation\',encoder.type,'\',flist(i).name);
                         sampleFeatFile = fullfile(pwd,'temp',sprintf('featfile_%d.mat',sample_size));
                         gmmFilePath = fullfile(codebook.path,flist(i).name,'\codebook\',sprintf('gmmModel_%d.mat',codebook.size));
                         codebookFilePath = fullfile(codebook.path,flist(i).name,'\codebook\',sprintf('codebook_%d.mat',codebook.size));
                         
%                          num_samples_per_dataset = ceil(sample_size/ num_time_series); 
                         dataname=names(randperm(numel(names)));
                         if ~exist(sampleFeatFile,'file')
                            descriptorFile = fullfile(descriptor_path,strcat(dataname{1},' '));
                            dt =load(descriptorFile);
                            All = zeros(sample_size,size(dt,2)-1);

                            warning('Generating encoder from subset of data...')

                            if num_time_series>numel(dataname), num_time_series = numel(dataname); end
                            st = 1;
                            for j = 1 :num_time_series
                                timest = tic();
                                descriptorFile = fullfile(descriptor_path,dataname{j});
                                dt =load(descriptorFile);

                                if ~isempty(dt)
                                    dt=dt(:,2:size(dt,2));
%                                     rnsam = randperm(size(dt,1));
%                                     if numel(rnsam) > num_samples_per_dataset
%                                         rnsam = rnsam(1:num_samples_per_dataset);
%                                     end
                                     send = st + numel(rnsam) - 1;
                                    All(st:send,:) = dt(rnsam,:);
                                end
                                 st = st + numel(rnsam); 
                                timest = toc(timest);
                                fprintf('%d/%d -> %s --> %1.2f sec\n',j,num_time_series,strcat(dataname{j}),timest);
                             end
                             if send ~= sample_size
                                All(send+1:sample_size,:) = [];
                             end

                         projectionCenter = mean(All',2) ;
                         projectionCenter = projectionCenter';
                         projectionCenter = bsxfun(@minus,All,projectionCenter);
                        fprintf('start saving sampling and projection center\n');
                        save(sampleFeatFile,'All','projectionCenter','-v7.3'); 


                         end
                         if exist(sampleFeatFile,'file')
                         load(sampleFeatFile);
                         end
                        if ~exist(codebookFilePath,'file')

                        fprintf('start create gmm & kmeans all\n');
                        codebook.words = vl_kmeans(All', codebook.size, 'verbose', 'algorithm', 'elkan') ;
                        codebook.means = vl_kmeans(projectionCenter', codebook.size, 'verbose', 'algorithm', 'elkan') ;
                        codebook.kdtree = vl_kdtreebuild(codebook.words) ;
                        [gmm.means, gmm.covariances, gmm.priors] = vl_gmm(projectionCenter', codebook.size);
                       
                        fprintf('start saving gmm and codebook\n');
                        save(gmmFilePath,'gmm');  
                        save(codebookFilePath, 'codebook');
                        end
                        if exist(codebookFilePath,'file')
                        load(gmmFilePath);
                        load(codebookFilePath);
                        end
                        fprintf(strcat(string(codebook.size),encoder.type,'-',flist(i).name,':starting the encoding process\n'));


                        for j = 1 :num_time_series
                             timest = tic();
                             savefilecsv=fullfile(midlevelpath,flist(i).name, sprintf('%s.csv',dataname{j}));
                            if ~exist(savefilecsv, 'file')
                                descriptorFile = fullfile(descriptor_path,dataname{j});
                                dt=[];
                                dt =load(descriptorFile);
                                dt=dt(:,2:size(dt,2));
                                if ~isempty(dt)
%%methods
%setencode
%                                     beta = 0.5; KNN = 5;
%                                     encoder.words=codebook.words; 
%                                     encoder.kdtree=codebook.kdtree;
%                                     encoder.numWords=codebook.numWords;
%                                     descrs=dt';
%                                     encoder.means=codebook.means;
                                         if string(encoder.type)=='fv'
                                            beta = 0.5; KNN = 5;
                                            
                                            encoder.means=gmm.means; 
                                            encoder.covariances=gmm.covariances;
                                            encoder.priors=gmm.priors;
                                           %%fazer pra todos?
                                            mdt = mean(dt',2) ;
                                            descrs=bsxfun(@minus,dt',mdt);
                                            use_vlfeat=1;
                                            code=encodeFeatures(descrs,encoder,use_vlfeat);

                                        elseif string(encoder.type)=='llc_sum'
                                            beta = 0.5; KNN = 5;
                                            encoder.words=codebook.words; 
                                            encoder.kdtree=codebook.kdtree;
                                            encoder.numWords=codebook.size;
                                            descrs=dt';
                                            encoder.means=codebook.means;
                                            code=encodeFeatures(descrs,encoder,0);

                                        elseif string(encoder.type)=='llc_max'
                                            beta = 0.5; KNN = 5;
                                            encoder.words=codebook.words; 
                                            encoder.kdtree=codebook.kdtree;
                                            encoder.numWords=codebook.size;
                                            descrs=dt';
                                            encoder.means=codebook.means;
                                            code=encodeFeatures(descrs,encoder,0);

                                        elseif string(encoder.type)=='sa-k_max'
                                            beta = 0.5; KNN = 5;
                                            encoder.words=codebook.words; 
                                            encoder.kdtree=codebook.kdtree;
                                            encoder.numWords=codebook.size;
                                            descrs=dt';
                                            code=encodeFeatures(descrs,encoder,0);
                                        elseif string(encoder.type)=='sa-k_sum'
                                            beta = 0.5; KNN = 5;
                                            encoder.words=codebook.words; 
                                            encoder.kdtree=codebook.kdtree;
                                            encoder.numWords=codebook.size;
                                            descrs=dt';
                                            code=encodeFeatures(descrs,encoder,0);
                                        elseif string(encoder.type)=='vq'
                                            beta = 0.5; KNN = 5;
                                            encoder.words=codebook.words; 
                                            encoder.kdtree=codebook.kdtree;
                                            encoder.numWords=codebook.size;
                                            descrs=dt';
                                            encoder.means=codebook.means;
                                            code=encodeFeatures(descrs,encoder,0);
                                        elseif string(encoder.type)=='vlad-k'
                                            beta = 0.5; KNN = 5;
                                            encoder.words=codebook.words; 
                                            encoder.kdtree=codebook.kdtree;
                                            encoder.numWords=codebook.size;
                                            descrs=dt';
                                            encoder.means=codebook.means;
                                            code=encodeFeatures(descrs,encoder,0);
                                        else
                                            return
                                        end
                                fid = fopen(strcat(classes_path,'\classes.txt'),'r');
                                tline = fgetl(fid);
                                count =0;
                                while ischar(tline)
                                    count=count+1;
                                    if count ==1
                                    trainLabels=strsplit(tline,' ');
                                    end
                                    if count ==2
                                    testLabels=strsplit(tline,' ');
                                    end
                                    tline = fgetl(fid);
                                end
                                fclose(fid);

                                nam=strsplit(dataname{j},'_');
                                if string(nam{1}) =='train'
                                idx=str2double(nam{2})+1;
                                idx=trainLabels(idx);
                                vec=[str2double(idx) code];
                                dlmwrite(savefilecsv,vec);
                                end
                                if string(nam{1}) =='test'
                                idx=str2double(nam{2})+1;
                                idx=testLabels(idx);
                                vec=[str2double(idx) code];
                                dlmwrite(savefilecsv,vec);
                                end
                                else
                                  fprintf('test');

                                end
                            timest = toc(timest);
                            fprintf('%d/%d -> %s -->  %1.1f sec.\n',j,num_time_series,dataname{j},timest);
                            end

                   end
               end
                
            end
    end

     
     
           
end

end
